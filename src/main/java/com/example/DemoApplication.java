package com.example;

import lombok.Data;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpMethod;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.http.Http;
import org.springframework.integration.dsl.scripting.Scripts;
import org.springframework.integration.dsl.support.Transformers;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.support.GenericMessage;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(DemoApplication.class, args);
        RentalService rentalService = context.getBean(RentalService.class);
        System.out.println(rentalService.findPlants("exc", LocalDate.now(), LocalDate.now().plusWeeks(1)));
        System.out.println(rentalService.findPlants("truck", LocalDate.now(), LocalDate.now().plusWeeks(1)));
    }

    @Bean
    IntegrationFlow scatter() {
        return IntegrationFlows.from("requestChannel")
                .publishSubscribeChannel(conf -> conf.applySequence(true)
                        .subscribe(flow -> flow.channel("requestChannel-rentit"))
                        .subscribe(flow -> flow.channel("requestChannel-rentmt")))
                .get();
    }

    @Bean
    IntegrationFlow gather() {
        return IntegrationFlows.from("gatherChannel")
                .aggregate(spec -> spec.outputProcessor(proc ->
                        {
                            List<PlantInventoryEntryDTO> list = new ArrayList<>();
                            for (Message m : proc.getMessages()) {
                                list.addAll(Arrays.asList((PlantInventoryEntryDTO[]) m.getPayload()));
                            }

                            return new GenericMessage<>(list);
                        }
//                                new GenericMessage<>(
//                                        proc.getMessages().stream()
//                                                .flatMap(msg -> Arrays.stream((PlantInventoryEntryDTO[]) msg.getPayload()))
//                                                .collect(Collectors.toList()))

                ))
                .channel("replyChannel")
                .get();
    }

    @Bean
    IntegrationFlow outboundHttpGatewayRentMT() {
        return IntegrationFlows.from("requestChannel-rentmt")
                .handle(Http
                        .outboundGateway("http://localhost:3030/rest/plants")
                        .httpMethod(HttpMethod.GET)
                        .expectedResponseType(String.class))
                .channel("siren2halChannel")
                .get();
    }

    @Bean
    IntegrationFlow outboundHttpGatewayRentIT() {
        return IntegrationFlows.from("requestChannel-rentit")
                .handle(Http
                        .outboundGateway("http://localhost:3000/api/inventory/plants")
                        .httpMethod(HttpMethod.GET)
                        .expectedResponseType(PlantInventoryEntryDTO[].class))
                .channel("gatherChannel")
                .get();
    }

    @Bean
    IntegrationFlow siren2halTransformation() {
        return IntegrationFlows.from("siren2halChannel")
                .transform(Scripts.script(new ByteArrayResource((
                        "print(payload);" +
                                "var array = JSON.parse(payload);" +
                                "var newArray = array.entities.map( function(e) {" +
                                "return { name: e.properties.name, description: e.properties.description, price: e.properties.price, _links:{self:{href: e.links[0].href}} }; " +
                                "});" +
                                "JSON.stringify( newArray )"
                ).getBytes()
                ))
                        .lang("javascript"))
                .transform(Transformers.fromJson(PlantInventoryEntryDTO[].class))
                .channel("gatherChannel")
                .get();
    }

}

@Data
class PlantInventoryEntryDTO {
    String name;
    String description;
    BigDecimal price;
}

@MessagingGateway
interface RentalService {
    @Gateway(requestChannel = "requestChannel", replyChannel = "replyChannel")
    Object findPlants(@Payload String name,
                      @Header("startDate") LocalDate startDate,
                      @Header("startDate") LocalDate endDate);
}

//@Service
//class RentalServiceImpl implements RentalService {
//
//    @Override
//    public Object findPlants(String name, LocalDate startDate, LocalDate endDate) {
//        return new RestTemplate().getForObject("http://localhost:3000/api/inventory/plants", String.class);
//    }
//}
//
//@Configuration
//class ScatterGather {
//
//
//}

